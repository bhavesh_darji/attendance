<?php

namespace Attend\Bundle\UserBundle\Controller;

use Attend\Bundle\UserBundle\Entity\User;
use Attend\Bundle\UserBundle\Form\Type\ChangePasswordType;
use Attend\Bundle\UserBundle\Form\Type\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{

    /**
     * @Route("/", name="home")
     */

    public function indexAction() {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render("AttendUserBundle:User:index.html.twig",array('user' => $user));
    }

    /**
     * @Route("/login", name="login")
    */
    public function loginAction(Request $request) {

        $authentication = $this->get('security.authentication_utils');
        $error          = $authentication->getLastAuthenticationError();
        $lastUsername   = $authentication->getLastUsername();

        return $this->render('AttendUserBundle:Security:login.html.twig',array(
            'last_username' => $lastUsername,
            'error'          => $error
        ));
    }

    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setIsActive(0);
            $user->setIsDelete(0);
            $user->setRole('ROLE_USER');
            $code = md5(uniqid(rand()));
            $user->setTokenCode($code);

            try{
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $userId = $user->getId();
                $key = base64_encode($userId);
                $userId = $key;
                $message = \Swift_Message::newInstance()
                    ->setSubject('User Registration')
                    ->setFrom('phpwithwp@gmail.com')
                    ->setTo('phpwithwp@gmail.com')
                    ->setBody(
                        $this->renderView(
                            'AttendUserBundle:Security:email.html.twig',
                            array(
                                'name' => $user->getFirstName(),
                                'code' => $code,
                                'userId' => $userId
                                )
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
            }

            $this->get('session')->getFlashBag()->set('success', ' We have sent an email to '.$user->getEmail().'
                    Please click on the confirmation link in the email to create your account...!');
        }
        return $this->render('AttendUserBundle:Security:register.html.twig', array('form'=> $form->createView(),'error' => array()));

    }

    /**
     * @Route("/forgot_password", name="forgot-password")
     * @param Username or Email
     * @return Update password and send new password in email
     */
    public function forgotPasswordAction(Request $request)
    {
        $authentication = $this->get('security.authentication_utils');
        $error          = $authentication->getLastAuthenticationError();
        $lastUsername   = $authentication->getLastUsername();

        $em = $this->getDoctrine()->getManager();
        $username = $request->request->get('username_email');
        $userQuery = $em->getRepository('AttendUserBundle:User')
            ->createQueryBuilder('u')
            ->where('u.username = :username')
            ->orWhere('u.email = :username')
            ->setParameter('username', $username)
            ->getQuery();
        $user = $userQuery->getResult();

        if ($user) {

            $Id = $user[0]->getId();
            $key    = base64_encode($Id);
            $userId = $key;

            $code = md5(uniqid(rand()));
           // $user->setTokenCode($code);

            $updateUser = $em->getRepository('AttendUserBundle:User')->find($Id);

            $updateUser->setTokenCode($code);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Password Reset')
                ->setFrom('phpwithwp@gmail.com')
                ->setTo('phpwithwp@gmail.com')
                ->setBody(
                    $this->renderView(
                        'AttendUserBundle:Security:forgot_pass_email.html.twig',
                        array(
                            'name' => $updateUser->getFirstName(),
                            'code' => $code,
                            'userId' => $userId
                        )
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
            $this->get('session')->getFlashBag()->set('success', 'We have sent an email to '.$updateUser->getEmail().'. Please click on the password reset link in the email to generate new password. ');

            return $this->render('AttendUserBundle:Security:login.html.twig', array(
                'last_username' => $lastUsername,
                'error'          => $error
            ));

        } else {
            return $this->render('AttendUserBundle:Security:forgot_password.html.twig', array(
                'last_username' => $lastUsername,
                'error'          => $error
            ));
        }
    }

    /**
     * @Route("/change_password/{id}/{code}", name="change-password")
    */
    public function changePasswordAction(Request $request, $id, $code) {

        $userId = base64_decode($id);
        $em             = $this->getDoctrine()->getManager();
        $authentication = $this->get('security.authentication_utils');
        $error          = $authentication->getLastAuthenticationError();
        $lastUsername   = $authentication->getLastUsername();

            $userData = $em->getRepository('AttendUserBundle:User')->findOneBy(array('id' => $userId , 'tokenCode' => $code ));

            if(count($userData) > 0 ) {

                $user = new User();
                $form = $this->createForm(ChangePasswordType::class,$user);
                $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {
                    echo "fsdfsdf"; exit;
                    $changePassword = $em->getRepository('AttendUserBundle:User')->find($userId);
                    $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
                    $changePassword->setPassword($password);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($changePassword);

                    $this->get('session')->getFlashBag()->set('success', 'Password Change Successfully. ');
                    return $this->render('AttendUserBundle:Security:login.html.twig', array(
                        'last_username' => $lastUsername,
                        'error'          => $error
                    ));
                }
            }
            return $this->render('AttendUserBundle:Security:password_reset.html.twig', array('form'=> $form->createView(),'userId' => $id,
                    'code' => $code, 'error' => array()));

    }

    /**
     * @Route("/verify/{id}/{code}", name="verify")
     */
    public function verifyAction(Request $request, $id , $code ) {

        $userId = base64_decode($id);
        $em             = $this->getDoctrine()->getManager();
        $authentication = $this->get('security.authentication_utils');
        $error          = $authentication->getLastAuthenticationError();
        $lastUsername   = $authentication->getLastUsername();

        try {
            $userData = $em->getRepository('AttendUserBundle:User')->findOneBy(array('id' => $userId , 'tokenCode' => $code ));

            if(count($userData) > 0 ) {
                if($userData->getIsActive()) {

                    $this->get('session')->getFlashBag()->set('error', 'sorry ! Your Account is allready Activated');
                } else {

                    $updateUser = $em->getRepository('AttendUserBundle:User')->find($userId);
                    $updateUser->setIsActive(true);
                    $em->flush();
                    $this->get('session')->getFlashBag()->set('success', 'WoW ! Your Account is Now Activated');

                }
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            exit;
        }

        return $this->render('AttendUserBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'          => $error
        ));
    }

    /**
     * @Route("/logout", name="logout")
    */
    public function logoutAction(Request $request) {

    }

}

?>